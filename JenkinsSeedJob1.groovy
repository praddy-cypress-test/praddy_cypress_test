freeStyleJob('example11') {
  scm {
    git {
      remote {
        url('https://pratheban@bitbucket.org/praddy-cypress-test/praddy_cypress_test.git')
      }
      branch('master')
    }
  }
}
freeStyleJob('example21') {
  scm {
    git {
      remote {
        url('https://pratheban@bitbucket.org/praddy-cypress-test/praddy_cypress_test.git')
      }
      branch('master')
    }
  }
}
def jobs = ['example31', 'example41']

for (job in jobs) {
  freeStyleJob(job) {
    scm {
      git {
        remote {
          url('https://pratheban@bitbucket.org/praddy-cypress-test/praddy_cypress_test.git')
        }
        branch('master')
      }
    }
  }
}