/// <reference types="cypress" />

beforeEach(() => {
  cy.visit("/detail/11");
});

describe("hero Detail", () => {
  it("should edit name", () => {
    //check the text boxs input text
    cy.get("input").should("have.value", "Mr. Nice");

    //another way to do the input text value
    //.invoke('val') get the value from the input box... val is the text
    cy.get("input")
      .invoke("val")
      .then((val) => {
        //Then is a call back function where the val is the input box text and
        //then you are using chai way to check the value
        expect(val).to.equal("Mr. Nice");
      });
  });
});

/*
describe('hero asfdasdf mock ', () => {
    it('moc data', () => {
        cy.server();
        cy.route({
            method: "GET",
            url: '/api/heroes',
            response: [{"id": 11, "name": "Spider Pig"}]
        })
        cy.visit('dashboard');
        
        cy.get('.module.hero').should('have.length' , 1);

    })

    it('moc data with fixture', () => {
        cy.server();
        cy.route({
            method: "GET",
            url: '/api/heroes',
            response: 'fixture:oneHeroes'
        })
        cy.visit('dashboard');
        
        cy.get('.module.hero').should('have.length' , 1);

    })

    it('moc data with fixture and wait for the route to respond', () => {
        cy.server();
        cy.route({
            method: "GET",
            url: '/api/heroes',
            response: 'fixture:oneHeroes'
        }).as('hero11update')
        cy.visit('dashboard');
        
        cy.get('.module.hero').should('have.length' , 1);

        cy.wait('@hero11update')

    })
    
})
*/
