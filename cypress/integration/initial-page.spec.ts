/// <reference types="cypress" />

beforeEach(() => {
  cy.visit("/dashboard");
});
describe("dashboard", () => {
  it('has title "Tour of Heroes"', () => {
    cy.get("h1").should("contain.text", "Tour of Heroes");
    cy.title().should("equal", "Tour of Heroes");
    cy.get('[routerlink="/dashboard"][href="/dashboard"]').should(
      "contain.text",
      "Dashboard"
    );
    cy.get('[routerlink="/heroes"][href="/heroes"]')
      .parent("nav")
      .should("contain.text", "Heroes");
    cy.get("app-hero-search #search-component h4").should(
      "contain.text",
      "Hero Search"
    );
    cy.get("app-hero-search")
      .get("#search-component h4")
      .should("contain.text", "Hero Search");
  });

  it("can search", () => {
    cy.get("#search-box").type("na");

    //cy.get('#search-box').invoke('v')
    cy.get(".search-result li").should("have.length", 3);
    cy.get("#search-box").type("{backspace}");
    cy.get(".search-result li").should("have.length", 6);
    cy.get(".search-result li").contains("Mr. Nice").click();
    cy.url().should("contain", "/detail/");
  });
});
