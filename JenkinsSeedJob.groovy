freeStyleJob('example1') {
  scm {
    git {
      remote {
        url('https://pratheban@bitbucket.org/praddy-cypress-test/praddy_cypress_test.git')
      }
      branch('master')
    }
  }
}
freeStyleJob('example2') {
  scm {
    git {
      remote {
        url('https://pratheban@bitbucket.org/praddy-cypress-test/praddy_cypress_test.git')
      }
      branch('master')
    }
  }
}
def jobs = ['example3', 'example4']

for (job in jobs) {
  freeStyleJob(job) {
    scm {
      git {
        remote {
          url('https://pratheban@bitbucket.org/praddy-cypress-test/praddy_cypress_test.git')
        }
        branch('master')
      }
    }
  }
}